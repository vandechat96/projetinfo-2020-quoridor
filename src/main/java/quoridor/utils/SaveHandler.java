package quoridor.utils;

import org.json.*;
import quoridor.model.game.*;
import quoridor.model.game.player.Bot;
import quoridor.model.game.player.BotRandom;
import quoridor.model.game.player.Human;

import java.io.*;

public class SaveHandler {

    public static Game Load(File file){
        try {
            JSONObject save;
            InputStream credentialsStream = new FileInputStream(file);
            try {
                save = new JSONObject(new JSONTokener(credentialsStream));
            } catch (JSONException e){
                System.out.println("Erreur lors du chargement de la sauvegarde");
                return null;
            }


            JSONArray JPlayers = save.getJSONArray("players");
            Player[] players = new Player[JPlayers.length()];

            for (int i = 0; i < JPlayers.length(); i++) {
                JSONObject player = JPlayers.getJSONObject(i);
                Point[] winPos = new Point[player.getJSONArray("winPositionList").length()];

                for (int j = 0; j < player.getJSONArray("winPositionList").length(); j++) {
                    JSONObject point = player.getJSONArray("winPositionList").getJSONObject(j);
                    winPos[j] = new Point(point.getInt("x"),point.getInt("y"));
                }

                JSONObject coords = player.getJSONObject("coords");
                String type = player.getString("type");

                Player p = null;
                if(type.equals("Human")){
                    p = new Human(player.getString("name"),new Point(coords.getInt("x"),coords.getInt("y")),winPos);
                } else if(type.equals("BotRandom")){
                    p = new BotRandom(player.getString("name"),new Point(coords.getInt("x"),coords.getInt("y")),winPos);
                }


                assert p != null;
                p.setRemainingWalls(player.getInt("remainingWalls"));
                players[i] = p;
            }

            JSONArray JBoard = save.getJSONArray("board");

            Cell[][] board = new Cell[JBoard.length()][JBoard.getJSONArray(0).length()];
            for (int i = 0; i < JBoard.length(); i++) {
                for (int j = 0; j < JBoard.getJSONArray(0).length(); j++) {
                    JSONObject JCell = JBoard.getJSONArray(i).getJSONObject(j);
                    board[i][j] = new Cell(JCell.getString("name"));

                }
            }
            World world = new World(board);

            for (Player player:players){
                if(player.isBot()) {
                    Bot bot = (Bot) player;
                    bot.setWorld(world);
                }
            }

            for (int i = 0; i < JBoard.length(); i++) {
                for (int j = 0; j < JBoard.getJSONArray(0).length(); j++) {
                    JSONObject JCell = JBoard.getJSONArray(i).getJSONObject(j);
                    if (JCell.has("wallDirection")){
                        JSONObject wallDirection = JCell.getJSONObject("wallDirection");

                        int x = wallDirection.getInt("x"), y = wallDirection.getInt("y");

                        world.placeWall(new Point(j,i),new Direction(new Point(x,y)));
                    }

                }
            }


            world.setPlayerList(players);

            world.spawnPlayers();
            world.refreshCanPlaceWall();

            //world.printWorld();

            return new Game(world,save.getInt("playerTurn"));

        } catch (FileNotFoundException e) {
            System.out.println("Fichier non trouve");
        } catch (JSONException e){
            System.out.println("Erreur lors du chargement de la sauvegarde");
        }

        return null;

    }

    public static void Save(Game game,File path){
        JSONObject JGame = new JSONObject();

        JSONObject[][] map = new JSONObject[game.getWorld().getBoard().length][game.getWorld().getBoard()[0].length];
        for (int i = 0; i < game.getWorld().getBoard().length; i++) {
            for (int j = 0; j < game.getWorld().getBoard()[0].length; j++) {
                Cell cell = game.getWorld().getBoard()[j][i];
                map[j][i] = new JSONObject().put("name",cell.getName());

                if(cell.hasWall()){
                    int x = cell.getWallCell().getWall().getDirection().getX();
                    int y = cell.getWallCell().getWall().getDirection().getY();
                    Point coords = cell.getWallCell().getWall().getCoords();
                    map[coords.getY()][coords.getX()].put("wallDirection",new JSONObject().put("x",x).put("y",y));
                }
            }
        }
        JSONArray JMap = new JSONArray(map);

        JSONObject[] JPlayers = new JSONObject[game.getWorld().getPlayerList().length];
        for (int i = 0; i < game.getWorld().getPlayerList().length; i++) {
            Player player = game.getWorld().getPlayerList()[i];
            JPlayers[i]= new JSONObject().put("name",player.getPlayerName());
            JPlayers[i].put("winPositionList",player.getWinPositionList());
            JPlayers[i].put("remainingWalls",player.getRemainingWalls());
            JPlayers[i].put("coords",new JSONObject().put("x",player.getCoords().getX()).put("y",player.getCoords().getY()));
            JPlayers[i].put("bot",player.isBot());
            JPlayers[i].put("type",player.getClass().getSimpleName());
        }

        JGame.put("players",JPlayers);
        JGame.put("board",JMap);
        JGame.put("playerTurn",game.getPlayerTurn());
        writeFile(JGame,path.getAbsolutePath());
    }

    private static void writeFile(JSONObject data,String path) {

        try (FileWriter file = new FileWriter(path)) {

            file.write(data.toString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*private static void writeFile(JSONObject data){
        String path = getSavePath();
        writeFile(data,path+"/save.qsf");
    }*/

    public static String getSavePath(){
        String path;
        if (System.getProperty("os.name").toLowerCase().contains("win")){
            path = System.getenv("HOMEDRIVE")+System.getenv("HOMEPATH")+"\\Documents\\Game\\Quoridor\\saves";
        } else {
            path = System.getenv("HOME")+"/Game/Quoridor/saves";
        }
        File dir = new File(path);
        dir.mkdirs();

        return path;
    }
}
