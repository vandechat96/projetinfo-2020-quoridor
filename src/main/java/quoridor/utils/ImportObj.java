package quoridor.utils;

import com.interactivemesh.jfx.importer.ImportException;
import com.interactivemesh.jfx.importer.obj.ObjModelImporter;
import javafx.scene.shape.MeshView;
import quoridor.Main;

import java.net.URL;

public class ImportObj {
    public static MeshView getPion(){
        ObjModelImporter objImporter = new ObjModelImporter();
        try {
            URL modelURL = Main.class.getResource("/3dObj/pion.obj");
            objImporter.read(modelURL);
        } catch (ImportException e){
            System.out.println(e.getMessage());
        }
        MeshView[] meshViews = objImporter.getImport();
        double scale = 0.2;
        meshViews[0].setScaleX(scale);
        meshViews[0].setScaleY(scale);
        meshViews[0].setScaleZ(scale);
        return meshViews[0];
    }
}
