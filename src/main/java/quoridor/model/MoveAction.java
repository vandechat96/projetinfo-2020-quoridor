package quoridor.model;

import quoridor.model.game.Player;
import quoridor.model.game.Point;

public class MoveAction extends Action {
    private final Point nextPosition;
    private final Point oldPos;

    public MoveAction(Player player, int turn, Point[] points){
        super( player, turn, points);
        nextPosition = points[0];
        oldPos = points[1];
    }

    public Point getNextPosition() {
        return nextPosition;
    }

    public Point getOldPos() {
        return oldPos;
    }

    @Override
    public String getActionType() {
        return "MoveAction";
    }
}
