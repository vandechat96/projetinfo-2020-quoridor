package quoridor.model;

import javafx.geometry.Point2D;
import quoridor.model.game.Player;
import quoridor.model.game.Point;

public interface GraphicGameInterface {

    void placeWall(Point[] wallPos);
    void removeWall(Point[] wallPos);
    void placePlayer(Player player, Point2D coords);
    void movePlayer(Player player, Point2D coords);
}
