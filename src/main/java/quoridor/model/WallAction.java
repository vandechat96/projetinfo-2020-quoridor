package quoridor.model;

import quoridor.model.game.Player;
import quoridor.model.game.Point;

public class WallAction extends Action{
    private final Point wallPos;
    private final Point wallDirection;

    public WallAction(Player player, int turn, Point[] points){
        super(player, turn, points);
        wallPos = points[0];
        wallDirection =  points[1];
    }

    @Override
    public String getActionType() {
        return "WallAction";
    }

    public Point getWallPos() {
        return wallPos;
    }

    public Point getWallDirection() {
        return wallDirection;
    }
}
