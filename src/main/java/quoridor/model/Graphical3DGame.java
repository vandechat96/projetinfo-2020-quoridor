package quoridor.model;

import javafx.geometry.Point2D;
import javafx.scene.*;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.MeshView;
import javafx.scene.transform.Translate;
import quoridor.Main;
import quoridor.model.game.Player;
import quoridor.model.game.Point;
import quoridor.utils.CameraManager;
import quoridor.utils.ImportObj;

import java.lang.reflect.WildcardType;


public class Graphical3DGame implements GraphicGameInterface {

    private final Group root3D;
    private final double COEF;
    private final int playerNumber;


    public Graphical3DGame(Group root3D, int playerNumber,double coef){
        this.root3D = root3D;
        this.playerNumber= playerNumber;
        COEF = coef;
    }

    public void placePlayer(Player player, Point2D coords) {
        MeshView pion = ImportObj.getPion();
        final PhongMaterial yellowMaterial = new PhongMaterial();
        Color color = player.getColor();
        yellowMaterial.setDiffuseColor(color);
        yellowMaterial.setSpecularColor(color);
        pion.setMaterial(yellowMaterial);
        root3D.getChildren().add(pion);

        Translate t = new Translate();
        t.setY(5.6);
        pion.setTranslateX(coords.getY());
        pion.setTranslateZ(coords.getX());


        pion.getTransforms().addAll(t);

        player.setPlayer3d(pion);
    }

    public void movePlayer(Player player, Point2D coords){
        MeshView pion = player.getPlayer3d();
        pion.setTranslateX(2*COEF+COEF*coords.getY()-0.25);
        pion.setTranslateZ(2*COEF+COEF*coords.getX()-0.25);

    }


    public void placeWall(Point[] wallPos){
        Point coords = wallPos[0], direction = wallPos[1];
        Box wall = new Box(direction.getY()==0?0.06:1.15,0.5,direction.getX()==0?0.06:1.15);

        Translate t = new Translate();
        double tX = direction.getX()==0?coords.getY()* COEF + COEF *2:coords.getY()* COEF + COEF;
        double tZ = direction.getY()==1?coords.getX()* COEF + COEF :coords.getX()* COEF + COEF *2;
        t.setX(tX);
        t.setZ(tZ);
        t.setY(-0.25);
        wall.getTransforms().add(t);

        PhongMaterial redMaterial = new PhongMaterial();
        redMaterial.setSpecularColor(Color.RED);
        redMaterial.setDiffuseColor(Color.RED);
        wall.setMaterial(redMaterial);


        root3D.getChildren().add(wall);
    }

    @Override
    public void removeWall(Point[] wallPos) {

    }


    public void init(Pane pane3D,int WIDTH,int HEIGHT){
        //https://www.lri.fr/~cfleury/teaching/et3-info/ProjetJavaIHM-2018/ressources/Tutoriel_JavaFX3D_2018.pdf

        //creation material
        final PhongMaterial blueMaterial = new PhongMaterial();
        blueMaterial.setDiffuseColor(Color.BLACK);
        blueMaterial.setSpecularColor(Color.BLACK);
        final PhongMaterial yellowMaterial = new PhongMaterial();
        yellowMaterial.setDiffuseColor(Color.ORANGE);
        yellowMaterial.setSpecularColor(Color.ORANGE);
        final PhongMaterial cyanMaterial = new PhongMaterial();
        cyanMaterial.setDiffuseColor(Color.rgb(0,255,255));
        cyanMaterial.setSpecularColor(Color.rgb(0,255,255));
        cyanMaterial.setSelfIlluminationMap(new Image(Main.class.getResourceAsStream("/img/test.jpg")));
        final PhongMaterial redMaterial = new PhongMaterial();
        redMaterial.setDiffuseColor(Color.RED);
        redMaterial.setSpecularColor(Color.RED);


        //ne demandez pas d'ou ca sort c du pif total
        double coefT = (6.0/9)+(0.17)*(1-(Math.log(WIDTH) / Math.log(9)));
        Box cube = new Box(coefT*WIDTH,0.2,coefT*HEIGHT);
        cube.setMaterial(blueMaterial);
        Translate t = new Translate();
        t.setX(coefT*WIDTH/2);
        t.setZ(coefT*HEIGHT/2);
        cube.getTransforms().add(t);
        root3D.getChildren().add(cube);


        //axes
        /*
        Box axeX = new Box(50,0.05,0.05);
        Box axeY = new Box(0.05,50,0.05);
        Box axeZ = new Box(0.05,0.05,50);
        axeX.setMaterial(redMaterial);
        axeZ.setMaterial(greenMaterial);
        axeY.setMaterial(blueMaterial);
        root3D.getChildren().addAll(axeX,axeY,axeZ);
        */
        //quadriallage
        for (int x = 0; x <= WIDTH; x++) {
            Box axeX = new Box(coefT*WIDTH+0.01,0.2,0.1);
            axeX.setMaterial(cyanMaterial);
            t = new Translate();
            t.setX(coefT*WIDTH/2);
            t.setY(-0.01);
            t.setZ(COEF*(x+1));
            axeX.getTransforms().add(t);
            root3D.getChildren().add(axeX);
        }
        for (int y = 0; y <= HEIGHT; y++) {
            Box axeY = new Box(0.1,0.2,coefT*HEIGHT+0.01);
            axeY.setMaterial(cyanMaterial);
            t = new Translate();
            t.setZ(coefT*HEIGHT/2);
            t.setY(-0.01);
            t.setX(COEF*(y+1));
            axeY.getTransforms().add(t);
            root3D.getChildren().add(axeY);
        }
        //petits points
        /*PhongMaterial[] test = {blueMaterial,yellowMaterial};
        for (int i = -50; i < 50; i++) {
            for (int j = -50; j < 60; j++) {
                Sphere sphere = new Sphere(0.05);
                sphere.setTranslateX(i);
                sphere.setTranslateZ(j);
                sphere.setMaterial(test[Math.abs(i%2)]);
                root3D.getChildren().add(sphere);
            }
        }*/


        //test sphere
        /*
        for (int i = 0; i < playerNumber; i++) {
            placePlayer(yellowMaterial,new Point2D(COEF*1.5,COEF*1.5),player);
        }*/

        //creation camera
        PerspectiveCamera camera = new PerspectiveCamera(true);

        //creation lumiere
        Light.Spot light = new Light.Spot();
        //PointLight light = new PointLight(Color.WHITE);
        light.setX(-180);
        light.setY(-90);
        light.setZ(-120);
        //light.getScope().addAll(root3D);
        //root3D.getChildren().add(light);
        light.setSpecularExponent(2);
        Lighting lighting = new Lighting();
        lighting.setLight(light);
        lighting.setSurfaceScale(5.0);

        cube.setEffect(lighting);

        //creation et ajout subscne 3d
        SubScene subScene = new SubScene(root3D,500,500,true, SceneAntialiasing.BALANCED);
        subScene.setCamera(camera);
        subScene.setFill(Color.rgb(10,0,20));
        pane3D.getChildren().add(subScene);

        new CameraManager(camera,subScene,root3D);


    }
}
