package quoridor.model.game;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * World est la classe qui représente et permet d'intéragir
 * avec le plateau de jeu.
 */
public class World implements WorldViewer{

    private final Cell[][] board;
    private Player[] playerList;
    private final int width;
    private final int height ;

    /**
     * Constructeur de la classe world
     * @param board
     *         Tableau a 2 dimensions qui représente le plateau
     */
    public World(Cell[][] board) {
        width = board.length/2+1;
        height = board[0].length/2+1;
        this.board = board;
    }

    /**
     * Constructeur de la classe world
     * qui génère un board par défaut
     */
    public World(){
        this(World.defaultBoard());
    }


    /**
     * Fonction qui crée un tableau à 2 dimensions
     * prévue pour le jeu
     * @param size
     *         Taille d'un côté du plateau
     * @return Plateau de taille size x size
     */
    public static Cell[][] defaultBoard(int size){
        Cell[][] board = new Cell[size*2-1][size*2-1];
        for (int y =0; y < board.length; y++){
            if(y % 2 == 0){
                for (int x = 0; x < board[0].length; x++) {
                    if (x % 2 == 0){
                        board[y][x] = new Cell("Case");
                    } else {
                        board[y][x] = new Cell();
                    }
                }
            } else {
                for (int x = 0; x < board[0].length; x++) {
                    board[y][x] = new Cell();
                }
            }

        }
        return board;
    }

    /**
     * Fonction qui crée un tableau à 2 dimensions
     * prévue pour le jeu avec la taille par défaut des règles qui
     * est 9 x 9
     * @return Plateau de taille 9 x 9 (taille réel de 18 x 18)
     */
    public static Cell[][] defaultBoard(){
        return defaultBoard(9);
    }

    /**
     * Affiche le plateau actuel dans la console
     */
    public void printWorld(){
        Map<String, String> style = new HashMap<>();
        style.put("void", " ");
        style.put("Case", "C");
        style.put("wall", "M");
        System.out.println("  0 1 2 3 4 5 6 7 8");
        System.out.println();
        for (int y = 0; y < this.board.length; y++) {
            if (y % 2 == 0){
                System.out.print(y/2+" ");
            } else {
                System.out.print("  ");
            }
            for (int x = 0; x < this.board[0].length; x++) {
                if (style.containsKey(this.board[y][x].getName())){
                    if (this.board[y][x].hasPlayer()){
                        System.out.print(this.board[y][x].getPlayer().getPlayerName());
                    } else if (this.board[y][x].hasWall()){
                        System.out.print(style.get("wall"));
                    } else {
                        System.out.print(style.get(this.board[y][x].getName()));
                    }
                }
            }
            System.out.println();
        }
    }

    /**
     * Fait apparaitre les joueurs qui sont dans la liste des joueurs
     * sur le plateau
     */
    public void spawnPlayers(){
        for (Player player:this.playerList){
            this.board[player.getCoords().getY()][player.getCoords().getX()].setPlayer(player);
        }
    }

    /**
     * Fonction qui vérifie si un mouvement est possible
     * @param player
     *         Le joueur qui doit faire le mouvement
     * @param direction
     *         La direction du mouvement
     * @param step
     *         Le nombre de cell que le joueur doit parcourir
     * @return
     *     0 : le mouvement est impossible
     *     1 : Le mouvement est possible
     *     -1
     */
    public int tryMovePlayer(Player player, Direction direction,int step){
        if (direction.equals(new Point(0,0))) return 0; //Si la direction est nulle, le mouvement est impossible

        int x = player.getCoords().getX(), y = player.getCoords().getY();

        Point wallPos = new Point(x,y).add(direction);
        Point newPos = new Point(x,y).add(direction.multiply(step));

        if (notInBound(newPos) || this.board[wallPos.getY()][wallPos.getX()].hasWall()) return 0;

        if (this.board[newPos.getY()][newPos.getX()].hasPlayer()){
            wallPos = new Point(x,y).add(direction.multiply(3));
            newPos = new Point(x,y).add(direction.multiply(step*2));
            if (this.notInBound(new Point(wallPos.getX(),wallPos.getY())) || this.notInBound(new Point(newPos.getX(),newPos.getY()))) return -1;
            if (this.board[wallPos.getY()][wallPos.getX()].hasWall()|| this.board[newPos.getY()][newPos.getX()].hasPlayer()) {
                return -1;
            } else {
                return 2;
            }
        }
        return 1;
    }

    public boolean diagonalMovePlayer(Player player,Direction directionPlayer, Direction directionDiagonalPlayer) {
        int x = player.getCoords().getX(),  y = player.getCoords().getY();

        Point newPos = new Point(x,y).add(directionPlayer.multiply(2)).add(directionDiagonalPlayer.multiply(2));
        Point wallPos = new Point(x,y).add(directionPlayer.multiply(2)).add(directionDiagonalPlayer);

        if (directionDiagonalPlayer.equals(directionPlayer) || directionDiagonalPlayer.equals(directionPlayer.getInvertedDirection()) || notInBound(newPos)) return false;
        return !this.board[wallPos.getY()][wallPos.getX()].hasWall() && !this.board[newPos.getY()][newPos.getX()].hasPlayer();
    }

    public void movePlayer (Player player, Point newPosition){
        this.board[player.getCoords().getY()][player.getCoords().getX()].setPlayer(null);
        player.setCoords(newPosition);
        this.board[player.getCoords().getY()][player.getCoords().getX()].setPlayer(player);
    }

    public void refreshDirectionsCanMove (Player player){
        int step = 2;
        player.clearPositionList();
        ArrayList<Point> playerPositionList = player.getPositionList();
        for(Direction direction : Direction.getAllDirection()){
            int move = tryMovePlayer(player,direction, step);
            if (move == 1 || move == 2) {
                int x = player.getCoords().getX() + (direction.getX() * step * move);
                int y = player.getCoords().getY() + (direction.getY() * step * move);
                playerPositionList.add(new Point(x,y));
                player.setPositionList(playerPositionList);
            } else if (move == -1) {
                for(Direction diagonalDirection : direction.getPerpendicularDirection()){
                    if (diagonalMovePlayer(player,direction,diagonalDirection)) {
                        int x = player.getCoords().getX() + (direction.getX() * step) + (diagonalDirection.getX() * step);
                        int y = player.getCoords().getY() + (direction.getY() * step) + (diagonalDirection.getY() * step);
                        playerPositionList.add(new Point(x,y));
                        player.setPositionList(playerPositionList);
                    }
                }
            }
        }
    }

    public boolean notInBound(Point coords){
        double x = coords.getX(), y = coords.getY();
        return (x < 0 || x >= this.board[0].length || y < 0 || y >= this.board.length);
    }

    public boolean checkWin(Player player){
        for (Point winPosition:player.getWinPositionList()){
            if (winPosition.equals(player.getCoords())) return true;
        }
        return false;
    }

    public Player[] getPlayerList(){
        return this.playerList;
    }

    public void setPlayerList(Player[] playerList) {
        this.playerList = playerList;
    }

    public Wall getWallAtPosition(Point coords){
        return this.board[ coords.getY()][ coords.getX()].getWallCell().getWall();
    }

    public void removeWall(Wall wall){
        for (WallCell wallCell : wall.getWallCells()){
            this.board[ wallCell.getCoords().getY()][ wallCell.getCoords().getX()].setWallCell(null);
        }
    }

    public void placeWall(Point coords,Direction direction){
        Wall wall = new Wall(coords,direction);
        WallCell[] wallCells= new WallCell[3];
        for (int i = 0; i < 3; i++) {
            Point wallCoords= new Point( coords.getX()+(direction.getX()*i), coords.getY()+(direction.getY()*i));
            WallCell wallCell= new WallCell(wallCoords,wall);
            this.board[wallCoords.getY()][wallCoords.getX()].setWallCell(wallCell);
            wallCells[i]=wallCell;
        }
        wall.setWallCells(wallCells);
    }

    public boolean searchAWayToTheWin(Player player, Point coords, Direction pastDirection,ArrayList<Point> caseAlreadyCheck){
        Point maybeWall = coords.add(pastDirection);
        if (caseAlreadyCheck.contains(coords) || this.notInBound(coords) || this.board[ maybeWall.getY()][ maybeWall.getX()].hasWall())
            return false;
        if (Arrays.asList(player.getWinPositionList()).contains(coords))
            return true;

        caseAlreadyCheck.add(coords);

        return   (searchAWayToTheWin(player,coords.add(new Direction("H").multiply(2)),new Direction("B"),caseAlreadyCheck)
                ||searchAWayToTheWin(player,coords.add(new Direction("B").multiply(2)),new Direction("H"),caseAlreadyCheck)
                ||searchAWayToTheWin(player,coords.add(new Direction("D").multiply(2)),new Direction("G"),caseAlreadyCheck)
                ||searchAWayToTheWin(player,coords.add(new Direction("G").multiply(2)),new Direction("D"),caseAlreadyCheck));
        }
        
    public boolean canPlaceWall (Point coords,Direction direction) {
        int x = coords.getX();
        int y = coords.getY();

        Point newPos2 = coords.add(direction.multiply(2));
        int newY2 =  newPos2.getY();
        int newX2 =  newPos2.getX();

        Point newPos = coords.add(direction);
        int newY =  newPos.getY();
        int newX =  newPos.getX();

        if (this.notInBound(coords) || this.notInBound(newPos2) || !this.board[y][x].getName().equals("void") || this.board[y][x].hasWall() || (x%2) == (y%2)){
            return false;
        }
        return this.board[newY2][newX2].getName().equals("void") && !this.board[newY2][newX2].hasWall() && this.board[newY][newX].getName().equals("void") && !this.board[newY][newX].hasWall();
    }
    
    public boolean checkCanPlaceWallPlayer (Point coords,Direction direction){
        if (!this.canPlaceWall(coords,direction))
            return false;
        this.placeWall(coords,direction);
        int wallsOk = 0;
        for(Player player : this.playerList) {
            ArrayList<Point> caseAlreadyCheck = new ArrayList<>();
            if (this.searchAWayToTheWin(player, player.getCoords(),new Direction(""),caseAlreadyCheck))
                wallsOk = wallsOk + 1;
        }
        this.removeWall(this.getWallAtPosition(coords));
        return (wallsOk == this.playerList.length);
    }

    public void refreshCanPlaceWall () {
		for (int y=0;y<board.length;y++) {
			for (int x=0;x<board.length;x++) {
				this.board[y][x].clearCanPlaceWall();
				for(int i=0;i<=3;i++){
				        if (this.checkCanPlaceWallPlayer(new Point(x,y),Direction.getAllDirection()[i]))
				            //System.out.println("LOG :"+this.board);
						    this.board[y][x].addCanPlaceWall(Direction.getAllDirection()[i],i);
					}
				}
			}
		}
 
    public boolean playerPlaceWall(Point coords,Direction direction){
		int x = coords.getX();
        int y = coords.getY();
        
        if (notInBound(coords) || Arrays.stream(this.board[y][x].getCanPlaceWall()).noneMatch(direction::equals)){
			return false;
		} else {
            this.placeWall(coords,direction);
            return true;
		}
	}

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Cell[][] getBoard(){
        return this.board;
    }

}
