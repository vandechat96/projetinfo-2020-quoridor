package quoridor.model.game;


/**
 * Class that mange direction
 */
public class Direction extends Point{
    private final String charDirection;

    /**
     * Direction constructor
     *
     * @param charDirection
     *            The direction wanted, it can be H (Haut),B (Bas),D (Droite), G(Gauche)
     */
    public Direction(String charDirection){
        super();

        this.charDirection = charDirection;

        switch (charDirection){
            case "H":
                x=0;
                y=-1;
                break;
            case "B":
                x=0;
                y=1;
                break;
            case "D":
                x=1;
                y=0;
                break;
            case "G":
                x=-1;
                y=0;
                break;
            case "HD":
                x=1;
                y=-1;
                break;
            case "HG":
                x=-1;
                y=-1;
                break;
            case "BD":
                x=1;
                y=1;
                break;
            case "BG":
                x=-1;
                y=1;
                break;
            default :
                x=0;
                y=0;
        }
    }

    public Direction(Point coords){
        super(coords.getX(),coords.getY());
        String[][] charDir = {{"","G",""},{"H","","B"},{"","D",""}};

        charDirection = charDir[coords.getX()+1][coords.getY()+1];
    }



    /**
     * Return if directions are the same or not
     *
     * @param other
     *            an other direction
     * @return
     *            boolean true or false
     */
    public boolean equals(Direction other){
		if (other == null) return false;
        return this.charDirection.equals(other.getChar());
    }

    /**
     * Retrun direction coordinate
     *
     * @return
     *            Point2D of the coordinate
     */
    public String getChar() {
		return charDirection;
	}

	public Direction getInvertedDirection() {
        switch (this.charDirection) {
            case "H":
                return new Direction("B");
            case "B":
                return new Direction("H");
            case "D":
                return new Direction("G");
            case "G":
                return new Direction("D");
            default:
                return new Direction("");
        }
    }

    public Direction[] getPerpendicularDirection() {
        Direction[] directionList = new Direction[2];
        if (this.charDirection.equals("H") || this.charDirection.equals("B")){
            directionList[0] = new Direction("D");
            directionList[1] = new Direction("G");
            return directionList;
        } else if (this.charDirection.equals("D") || this.charDirection.equals("G")) {
            directionList[0] = new Direction("H");
            directionList[1] = new Direction("B");
            return directionList;
        } else
            return directionList;
    }


    public static Direction[] getAllDirection() {
        return new Direction[]{new Direction("H"),new Direction("B"),new Direction("D"),new Direction("G")};
    }
}

