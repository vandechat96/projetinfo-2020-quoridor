package quoridor.model.game.player;

import quoridor.model.game.Player;
import quoridor.model.game.Point;

public class Human extends Player {
    public Human(String playerName, Point coords, Point[] winPositionList) {
        super(playerName, coords, winPositionList);
    }
}
