package quoridor.model.game.player;

import quoridor.model.game.Player;
import quoridor.model.game.Point;
import quoridor.model.game.WorldViewer;


public class Bot extends Player {
    protected WorldViewer world;
    public Bot(String playerName, Point coords, Point[] winPositionList) {
        super(playerName, coords, winPositionList);
        bot = true;
    }

    public Point[] nextAction() {
        return new Point[0];
    }

    public void setWorld(WorldViewer world) {
        this.world = world;
    }
}
