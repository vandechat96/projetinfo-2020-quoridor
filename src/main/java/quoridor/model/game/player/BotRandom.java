package quoridor.model.game.player;

import quoridor.model.game.Direction;
import quoridor.model.game.Point;
import java.util.Random;

public class BotRandom extends Bot{
    public BotRandom(String playerName, Point coords, Point[] winPositionList) {
        super(playerName, coords, winPositionList);
    }
    public Point[] nextAction(){
        Random rand = new Random();
        int nbr = rand.nextInt(2);
        if (nbr == 1 || getRemainingWalls() <= 0){
            Point[] positions = getPositionList().toArray(new Point[0]);
            nbr = rand.nextInt(positions.length);
            return new Point[] {positions[nbr],null};
        } else {
            int x;
            int y;
            Direction direction;
            do{
                x = rand.nextInt(world.getHeight()*2);
                y = rand.nextInt(world.getWidth()*2);
                nbr = rand.nextInt(2);

                if (nbr == 0){
                    direction = new Direction("B");
                } else {
                    direction = new Direction("D");
                }
            } while (!world.checkCanPlaceWallPlayer(new Point(x,y),direction));

            return new Point[] {new Point(x,y),direction};
        }

    }
}
