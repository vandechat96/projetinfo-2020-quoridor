package quoridor.model.game;

import javafx.geometry.Point2D;

public class Point {
    protected int x;
    protected int y;
    
    private int hash = 0;

    public Point(){

    }
    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Point add(int x,int y){
        return new Point(this.x+x,this.y+y);
    }
    public Point add(Point other){
        return add(other.getX(),other.getY());
    }

    public Point multiply(int lambda){
        return new Point(x*lambda,y*lambda);
    }

    public Point divide(int lambda){
        return new Point(x/lambda,y/lambda);
    }

    public String toString(){
        return "Point : x="+x+" y="+y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Point2D toPoint2D(){
        return new Point2D(x,y);
    }


    //inspired from javafx Point2D
    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        
        if(obj instanceof Point) {
            Point other = (Point) obj;
            return (this.x == other.getX()) && (this.y == other.getY());
        } else return false;
    } 
    
    //inspired from javafx Point2D
    @Override
    public int hashCode() {
        if (hash == 0) {
            long bits = 7L;
            bits = 31L * bits + Double.doubleToLongBits(getX());
            bits = 31L * bits + Double.doubleToLongBits(getX());
            hash = (int) (bits ^ (bits >> 32));        
        }
        return hash;
    }
}

