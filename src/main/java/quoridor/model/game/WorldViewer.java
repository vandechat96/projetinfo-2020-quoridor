package quoridor.model.game;

public interface WorldViewer {
    boolean notInBound(Point coords);
    boolean checkWin(Player player);
    boolean checkCanPlaceWallPlayer (Point coords,Direction direction);
    Cell[][] getBoard();
    int getWidth();
    int getHeight();
}
