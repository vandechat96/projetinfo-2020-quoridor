package quoridor.model.game;

public class Wall {
    private final Point coords;
    private final Direction direction;
    private WallCell[] wallCells;

    public Wall(Point coords, Direction direction){
        this.coords = coords;
        this.direction = direction;
    }

    public Point getCoords() {
        return coords;
    }

    public Direction getDirection() {
        return direction;
    }

    public WallCell[] getWallCells() {
        return wallCells;
    }

    public void setWallCells(WallCell[] wallCells) {
        this.wallCells = wallCells;
    }
}
