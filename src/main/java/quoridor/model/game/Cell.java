package quoridor.model.game;

public class Cell {

    private final String name;
    private Player player;
    private WallCell wallCell;
    private Direction[] canPlaceWall = new Direction[4];


    public Cell(){
        this("void");
    }

    public Cell(String name){
        this.name = name;
    }

    public boolean hasPlayer(){
        return this.player != null;
    }

    public boolean hasWall(){
        return this.wallCell != null;
    }

    public String getName() {
        return name;
    }

    public Player getPlayer(){
        return this.player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public WallCell getWallCell(){
        return this.wallCell;
    }

    public void setWallCell(WallCell wallCell) {
        this.wallCell = wallCell;
    }
    
    public void addCanPlaceWall (Direction dir,int index) {
		this.canPlaceWall[index] = dir;
	}
	
	public boolean isEmptyCanPlaceWall () {
		for(Direction element:this.canPlaceWall){
			if(element != null){
				return false;
			}
		}
		return true;
	}
	
	public Direction[] getCanPlaceWall() {
		return this.canPlaceWall;
	}
	
	public void clearCanPlaceWall(){
        canPlaceWall = new Direction[4];
	}
}
