package quoridor.model.game;

import javafx.scene.paint.Color;
import javafx.scene.shape.MeshView;

import java.util.ArrayList;

public class Player {
    private final String playerName;
    private Point coords;
    private final Point[] winPositionList;
    private int remainingWalls;
    private ArrayList<Point> positionList = new ArrayList<>();
    protected boolean bot = false;
    private MeshView player3d;
    private Color color;

    public Player(String playerName, Point coords, Point[] winPositionList){
        this.playerName = playerName;
        this.coords = coords;
        this.winPositionList = winPositionList;
        this.remainingWalls = 10;
    }

    public String getPlayerName() {
        return playerName;
    }

    public Point getCoords() {
        return coords;
    }

    public void setCoords(Point coords) {
        this.coords = coords;
    }

    public Point[] getWinPositionList() {
        return winPositionList;
    }

    public boolean hasWall(){
        return getRemainingWalls()>0;
    }

    public int getRemainingWalls() {
        return remainingWalls;
    }

    public void setRemainingWalls(int remainingWalls) {
        this.remainingWalls = remainingWalls;
    }

    public ArrayList<Point> getPositionList() {
        return positionList;
    }

    public void setPositionList(ArrayList<Point> positionList) {
        this.positionList = positionList;
    }

    public void clearPositionList() {
        this.positionList.clear();
    }

    public boolean isBot(){
        return bot;
    }

    public MeshView getPlayer3d() {
        return player3d;
    }

    public void setPlayer3d(MeshView player3d) {
        this.player3d = player3d;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
