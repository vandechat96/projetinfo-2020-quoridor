package quoridor.model.game;

import com.sun.prism.shader.Solid_TextureYV12_AlphaTest_Loader;
import javafx.application.Platform;
import javafx.geometry.Point2D;
import quoridor.controller.GameSelectionController;
import quoridor.controller.GameViewController;
import quoridor.model.Action;
import quoridor.model.MoveAction;
import quoridor.model.WallAction;
import quoridor.model.game.player.BotRandom;
import quoridor.model.game.player.Human;
import quoridor.model.game.player.Bot;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Game {
    private final World world;
    private GameViewController gameViewController;


    private Player currentPlayer;
    private int playerTurn;


    //cree un jeux avec un monde et un tour choisi
    public Game(World world,int playerTurn){
        this.world = world;
        this.playerTurn = playerTurn;
        if (world.getPlayerList() != null){
            currentPlayer = world.getPlayerList()[playerTurn];
        }

    }
    //cree un jeux avec un monde choisi
    public Game(World world){
       this(world,0);
        if (currentPlayer == null && world.getPlayerList() != null){
            currentPlayer = world.getPlayerList()[0];
        }
    }
    //cree un jeu de base selon certain joueurs
    public Game(ArrayList<GameSelectionController.PlayerAttribute> playerAttributes,int size,int nbrMurs){
        this(new World( World.defaultBoard(size)));

        int maxsize = (size-1)*2;
        int midsize = size-1;
        int i=0;
        Player[] players= new Player[playerAttributes.size()];
        for(GameSelectionController.PlayerAttribute playerAttribute:playerAttributes){
            Point[] pWinPos = new Point[size];
            for (int j = 0; j < size; j++) {

                int x =i<2?j*2:((i)%2)*maxsize;
                int y =i<2?((i)%2)*maxsize:j*2;
                pWinPos[j] = new Point(x,y);
            }
            int x = i<2?midsize:((i+1)%2)*maxsize;
            int y = i<2?((i+1)%2)*maxsize:midsize;

            if(playerAttribute.type.equals("human")){
                Player player = new Human(playerAttribute.name,new Point(x,y),pWinPos);
                player.setRemainingWalls(nbrMurs);
                players[i++] = player;
            } else {
                Bot player = new BotRandom(playerAttribute.name,new Point(x,y),pWinPos);
                player.setRemainingWalls(nbrMurs);
                player.setWorld(world);
                players[i++] = player;
            }

        }

        world.setPlayerList(players);

        world.spawnPlayers();
        world.refreshCanPlaceWall();
        for (Player player:world.getPlayerList()){
            world.refreshDirectionsCanMove(player);
        }
        currentPlayer = world.getPlayerList()[0];
    }
    //cree un jeux de base
    public Game(){
        this(new World());
        Point[] p1WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p1WinPos[x] = new Point(x*2,16);
        }

        Player p1 = new Human("Alice",new Point(8,0),p1WinPos);

        Point[] p2WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p2WinPos[x] = new Point(x*2,0);
        }

        Bot p2 = new BotRandom("Bob",new Point(8,16),p2WinPos);
        p2.setWorld(world);


        Player[] players = {p1,p2};
        world.setPlayerList(players);

        world.spawnPlayers();
        world.refreshCanPlaceWall();
        for (Player player:world.getPlayerList()){
            world.refreshDirectionsCanMove(player);
        }
        currentPlayer = world.getPlayerList()[0];
    }

    //set du controleur de jeu et maj label
    public void setGameViewController(GameViewController gameViewController){
        this.gameViewController = gameViewController;
        this.gameViewController.getMainController().getRootLayoutController().setLabel("Player "+ currentPlayer.getPlayerName()+" "+ currentPlayer.getRemainingWalls()+" mur(s) restant.");
    }

    //passe au tour suivant et verifie si gagner
    public synchronized void nextTurn(Point[] points){
        this.getWorld().refreshDirectionsCanMove(currentPlayer);
        Action action;
        if (points[1] != null){
            //essaye de poser le mur
            if(!this.getWorld().playerPlaceWall(points[0],new Direction(points[1]))) return;

            action = new WallAction(currentPlayer,playerTurn,points);

            currentPlayer.setRemainingWalls(currentPlayer.getRemainingWalls()-1);
        } else {
            //bouge le perso
            if(!currentPlayer.getPositionList().contains(points[0])) return;

            Point oldPos = currentPlayer.getCoords();
            this.getWorld().movePlayer(currentPlayer,points[0]);

            action = new MoveAction(currentPlayer,playerTurn, new Point[]{points[0], oldPos});
        }

        Runnable task = () -> {
            gameViewController.updateGui(action);
        };

        Platform.runLater(task);


        if(world.checkWin(currentPlayer)){
            gameViewController.getG2D().drawWin(currentPlayer.getPlayerName());
        }


        world.refreshCanPlaceWall();
        for (Player player:world.getPlayerList()){
            world.refreshDirectionsCanMove(player);
        }
        playerTurn = (playerTurn +1)%world.getPlayerList().length;
        currentPlayer = world.getPlayerList()[playerTurn];

        gameViewController.print("Player "+ currentPlayer.getPlayerName()+" "+ currentPlayer.getRemainingWalls()+" mur(s) restant.");

        if (currentPlayer.isBot()) {
            Thread t = new Thread(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                nextTurn(((Bot) currentPlayer).nextAction());
            });
            t.start();


        }
    }



    //lance le jeu en console
    public void consoleGame(){
        World world = new World();
        //world.defaultMap();

        Point[] p1WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p1WinPos[x] = new Point(x*2,16);
        }

        Player p1 = new Player("1",new Point(8,0),p1WinPos);

        Point[] p2WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p2WinPos[x] = new Point(x*2,0);
        }

        Player p2 = new Player("2",new Point(8,16),p2WinPos);

        Player[] players = {p1,p2};
        world.setPlayerList(players);

        world.spawnPlayers();
		world.refreshCanPlaceWall();
        boolean game = true;

        while (game){
            for (Player player:world.getPlayerList()){
                world.printWorld();

                Scanner in = new Scanner(System.in);


                String[] values = {"Move","Wall","M","W","T","TEST"};

                String action;
                do {
                    System.out.println("Move or Wall ?\n");
                    action = in.nextLine();

                } while (Arrays.stream(values).noneMatch(action::equals));

                switch (action){
                    case "Wall":
                    case "W":
                        int wallPosX;
                        int wallPosY;
                        String direction;
                        Direction wallDirection;
                        do {
                            System.out.println("Wall pos x");
                            wallPosX = in.nextInt();
                            System.out.println("Wall pos y");
                            wallPosY = in.nextInt();
                            System.out.println("Direction");
                            direction = in.next();
                            wallDirection = new Direction(direction);
                        }  while (!world.playerPlaceWall(new Point(wallPosX,wallPosY),wallDirection));
                        world.refreshCanPlaceWall();
                        break;
                    case "M":
                    case "Move":
                        world.refreshDirectionsCanMove(player);
                        boolean notMoving = true;
                        do {
                            System.out.println("p"+player.getPlayerName()+" move :");
                            direction = in.next();
                            Direction playerDirection = new Direction(direction);
                            Point playerMove = player.getCoords().add(playerDirection.multiply(2));
                            Point playerMoveWithPlayer = player.getCoords().add(playerDirection.multiply(4));
                            if (player.getPositionList().contains(playerMove)){
                                world.movePlayer(player, playerMove);
                                notMoving = false;
                            } else if (player.getPositionList().contains(playerMoveWithPlayer)) {
                                world.movePlayer(player, playerMoveWithPlayer);
                                notMoving = false;
                            }
                        }while (notMoving);
                        break;
                    case "T":
                        ArrayList<Point> caseAlreadyCheck = new ArrayList<>();
                        System.out.println(world.searchAWayToTheWin(player, player.getCoords(),new Direction(""),caseAlreadyCheck));
                }
                if (world.checkWin(player)){
                    game = false;
                    world.printWorld();
                    System.out.println("p"+player.getPlayerName()+"Win");
                    break;
                }
            }
        }
    }

    public World getWorld() {
        return world;
    }

    public int getPlayerTurn(){
        return playerTurn;
    }

    public Player getCurrentPlayer(){
        return this.currentPlayer;
    }
}
