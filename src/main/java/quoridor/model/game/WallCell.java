package quoridor.model.game;


public class WallCell {
    private final Point coords;
    private final Wall wall;

    public WallCell(Point coords, Wall wall){
        this.coords = coords;
        this.wall = wall;
    }

    public Wall getWall() {
        return wall;
    }

    public Point getCoords() {
        return coords;
    }
}
