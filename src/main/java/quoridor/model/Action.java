package quoridor.model;

import quoridor.model.game.Player;
import quoridor.model.game.Point;

public abstract class Action {
    protected final Player player;
    protected final int turn;
    protected final Point[] points;

    protected Action(Player player, int turn, Point[] points) {
        this.player = player;
        this.turn = turn;
        this.points = points;
    }

    public abstract String getActionType();

    public Player getPlayer() {
        return player;
    }

}
