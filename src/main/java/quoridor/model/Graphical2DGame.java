package quoridor.model;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.transform.Affine;
import quoridor.model.game.Player;
import quoridor.model.game.Point;

public class Graphical2DGame implements GraphicGameInterface{

    private static final double LINE_WIDTH=0.15;
    private final GraphicsContext g;


    public Graphical2DGame(GraphicsContext g){
        this.g = g;
    }

    public void drawCanMove(Point coords){
        g.setFill(Color.GREY);
        double taille = 0.5;
        g.fillRect(coords.getX()+taille/2,coords.getY()+taille/2,taille,taille);
    }

    public void delCanMove(Point coords){
        g.setFill(Color.BLACK);
        double taille = 0.5;
        g.fillRect(coords.getX()+(taille-0.1)/2,coords.getY()+(taille-0.1)/2,taille+0.1,taille+0.1);
    }

    public void init(Affine affine,int WIDTH,int HEIGHT) {
        g.setFill(Color.BLACK);
        g.setTransform(affine);
        g.fillRect(0, 0, WIDTH, HEIGHT);


        g.setStroke(Color.CYAN);
        g.setLineWidth(LINE_WIDTH);

        for (int x = 1; x <  HEIGHT; x++) {
            g.strokeLine(x, 0, x, HEIGHT);
        }

        for (int y = 1; y < WIDTH; y++) {
            g.strokeLine(0, y, WIDTH, y);

        }


    }

    public void placePlayer(Player player, Point2D coords){
        g.setFill(player.getColor());
        double taille = 0.5;
        g.fillOval(coords.getX()+taille/2,coords.getY()+taille/2,taille,taille);
    }

    @Override
    public void movePlayer(Player player, Point2D coords) {
        removePlayer(player,coords);
        placePlayer(player,player.getCoords().divide(2).toPoint2D());
    }

    public void removePlayer(Player player,Point2D coords){
        g.setFill(Color.BLACK);
        double taille = 0.5;
        g.fillOval(coords.getX()+(taille-0.1)/2,coords.getY()+(taille-0.1)/2,taille+0.1,taille+0.1);
    }


    public void placeWall(Point[] wallPos){
        int x = wallPos[0].getX(), y = wallPos[0].getY(),a = wallPos[1].getX(),b =wallPos[1].getY();
        g.setStroke(Color.FUCHSIA);
        final double epaisseur = 0.07;
        g.setLineWidth(epaisseur);
        if (a == 0) {
            g.fillRect(x-LINE_WIDTH/2, y+LINE_WIDTH/2, LINE_WIDTH, b*2-LINE_WIDTH);
            //g.strokeRect(x-LINE_WIDTH/2, y+LINE_WIDTH/2, LINE_WIDTH, b*2-LINE_WIDTH);
        } else if (b ==0) {
            g.fillRect(x+LINE_WIDTH/2, y-LINE_WIDTH/2, a*2-LINE_WIDTH, LINE_WIDTH);
            //g.strokeRect(x+LINE_WIDTH/2, y-LINE_WIDTH/2, a*2-LINE_WIDTH, LINE_WIDTH);
        }


    }

    public void removeWall(Point[] wallPos) {
        if (wallPos == null) return;
        g.setFill(Color.CYAN);
        placeWall(wallPos);
    }

    public void drawWin(String name){
        g.setFill(Color.SPRINGGREEN);
        g.setFont(new Font("Arial",2));
        g.fillText("Player "+ name+" win.",0,5,9);
    }
    public void setDrawColor(Color color){
        g.setFill(color);
    }
}
