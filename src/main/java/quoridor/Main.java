package quoridor;

import javafx.application.Application;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import quoridor.controller.*;
import quoridor.model.game.Game;



public class Main extends Application {


    @Override
    public void start(Stage stage) {

        Font.loadFont(Main.class.getClassLoader().getResourceAsStream( "./fonts/Pixellari.ttf"),10);
        Font.loadFont(Main.class.getClassLoader().getResourceAsStream( "./fonts/TRON.ttf"), 10);

        new MainController(stage);
    }

    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("console")){
            Game game = new Game();
            game.consoleGame();
        } else{
            launch();
        }

    }




}
