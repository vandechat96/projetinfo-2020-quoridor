package quoridor.controller;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import quoridor.Main;
import quoridor.model.game.Game;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class MainController {
    private final Stage primaryStage;
    private BorderPane rootLayout;
    private VBox gameMenu;
    private RootLayoutController rootLayoutController;
    private AnchorPane gameView;
    private StackPane basePane;
    private GameMenuController gameMenuController;
    private AnchorPane optionsMenu;
    private OptionsMenuController optionsMenuController;
    private final LinkedList<String> currentPage = new LinkedList<>();
    private final Image icon = new Image(Main.class.getResourceAsStream("/img/ico.png"));
    private static String dp = ":";
    private ScheduledFuture<?> scheduledFuture;
    private final Label heure = new Label();
    private final ArrayList<Controller> controllers = new ArrayList<>();
    private JSONObject localization;
    private Game game;


    public MainController(Stage stage){
        this.primaryStage = stage;

        this.primaryStage.getIcons().add(icon);
        this.primaryStage.setTitle("Quoridor");
        primaryStage.setOnCloseRequest(event -> {
            exitDialog();
            event.consume();
        });
        primaryStage.setResizable(false);
        loadOptions();
        initRootLayout();
        showStartOverview();
        currentPage.add("StartOverview");
        Platform.setImplicitExit(false);
    }


    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     *
     */
    public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/fxml/RootLayout.fxml"));
            rootLayout = loader.load();
            basePane = new StackPane();
            basePane.getChildren().add(rootLayout);
            Scene scene = new Scene(basePane);
            primaryStage.setScene(scene);
            primaryStage.show();
            rootLayoutController = loader.getController();
            rootLayoutController.setMainController(this);

            updateTime();
            heure.setMaxHeight(10);
            rootLayout.setTop(heure);
            controllers.add(rootLayoutController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void showStartOverview() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/fxml/StartMenu.fxml"));
            AnchorPane startMenu = loader.load();
            rootLayout.setCenter(startMenu);
            StartMenuController controller = loader.getController();
            controller.setMainController(this);
            controllers.add(controller);
            initOptions();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void showGameView(Game game) {
        this.game = game;
        currentPage.add("GameView");
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/fxml/GameView.fxml"));
            gameView = loader.load();
            rootLayout.setCenter(gameView);
            GameViewController controller = loader.getController();
            controller.setMainController(this);
            rootLayoutController.getBackButton().setVisible(true);
            controllers.add(rootLayoutController);
            initMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

     public void showGameView() {
        showGameView(new Game());
    }

    public void showGameSelection() {
        currentPage.add("GameSelection");
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/fxml/GameSelection.fxml"));
            AnchorPane gameSelection = loader.load();
            rootLayout.setCenter(gameSelection);
            GameSelectionController controller = loader.getController();
            controller.setMainController(this);
            rootLayoutController.getBackButton().setVisible(true);
            controllers.add(rootLayoutController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void back(){
        currentPage.removeLast();
        rootLayoutController.getBackButton().setVisible(false);
        rootLayoutController.setLabel("");
        gameView =null;
        if(gameMenu != null) gameMenu.setVisible(false);
        //gameView.setDisable(true);
        showStartOverview();
    }
    public void exitDialog(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(localization.getString("ExitTitle"));
        alert.setHeaderText(localization.getString("ExitHeader"));
        alert.setContentText(localization.getString("ExitContent"));
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(icon);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(Main.class.getResource("/styles/dialog.css").toExternalForm());
        dialogPane.getStyleClass().add("myDialog");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            scheduledFuture.cancel(true);
            Platform.exit();
        }
    }
    private void initMenu(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/fxml/GameMenu.fxml"));
            gameMenu = loader.load();
            basePane.getChildren().add(gameMenu);
            gameMenu.setVisible(false);
            gameMenu.toBack();

            gameMenuController = loader.getController();
            gameMenuController.setMainController(this);
            controllers.add(gameMenuController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void initOptions(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/fxml/OptionsMenu.fxml"));
            optionsMenu = loader.load();
            basePane.getChildren().add(optionsMenu);
            optionsMenu.setVisible(false);
            optionsMenu.toBack();

            optionsMenuController = loader.getController();
            optionsMenuController.setMainController(this);
            controllers.add(optionsMenuController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadOptions(){
        setLocalization("english.json");
    }

    public void setLocalization(String language){
        try {
            String path = Main.class.getResource("/localization/"+language).getPath().replace("%20"," ");
            System.out.println();
            InputStream credentialsStream = new FileInputStream(path);
            try {
                localization = new JSONObject(new JSONTokener(credentialsStream));

            } catch (JSONException ex){
                ex.printStackTrace();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Controller controller:controllers){
            controller.setLocalization();
        }


    }

    public VBox getGameMenu(){
        return this.gameMenu;
    }

    public GameMenuController getGameMenuController(){
        return this.gameMenuController;
    }

    public AnchorPane getOptionsMenu(){
        return this.optionsMenu;
    }

    public OptionsMenuController getOptionsMenuController(){
        return this.optionsMenuController;
    }


    public LinkedList<String> getCurrentPage() {
        return currentPage;
    }


    public Game getGame() {
        return game;
    }

    public RootLayoutController getRootLayoutController() {
        return rootLayoutController;
    }
    public JSONObject getLocalization(){
        return localization;
    }

    public void updateTime() {
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(2);
        Runnable task2 = () -> {
            LocalDateTime now = LocalDateTime.now();
            String heure_ = now.getHour()<10?"0"+now.getHour(): ""+now.getHour();
            String minutes_ = now.getMinute()<10?"0"+now.getMinute(): ""+now.getMinute();
            dp = dp.equals(":")?" ":":";
            String txt = heure_+dp+minutes_;
            heure.setText(txt);
        };

        Runnable task1 = ()-> Platform.runLater(task2);

        scheduledFuture = ses.scheduleAtFixedRate(task1, 1, 1, TimeUnit.SECONDS);

    }

}
