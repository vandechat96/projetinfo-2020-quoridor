package quoridor.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.*;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;
import javafx.scene.transform.NonInvertibleTransformException;
import quoridor.model.*;
import quoridor.model.game.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;


public class GameViewController extends Controller{
    private Affine affine;
    private Graphical2DGame g2D;

    private static final double LINE_WIDTH=0.25;

    public static final double COEF = 2.2/4;

    private Game game;
    private Point[] oldWallPos = {new Point(0,0),new Point(0,0),new Point(0,0)};
    private ArrayList<Point> onMove;

    @FXML private Canvas canvas;

    @FXML private Pane pane3D;
    private final Group root3D = new Group();

    private Graphical3DGame g3D;


	public void setMainController(MainController mainController) {

        super.setMainController(mainController);

        game = mainController.getGame();
        game.setGameViewController(this);

        World world = game.getWorld();

        this.affine = new Affine();
        int HEIGHT = 500;
        int WIDTH = 500;
        this.affine.appendScale((double)HEIGHT / world.getHeight(), (double)WIDTH / world.getWidth());


        g2D =  new Graphical2DGame(canvas.getGraphicsContext2D());
        g2D.init(affine,world.getWidth(),world.getHeight());

        g3D = new Graphical3DGame(root3D,world.getPlayerList().length,COEF);
        g3D.init(pane3D,world.getWidth(),world.getHeight());

        loadGame();
    }



    private void loadGame(){
        final ArrayList<Color> colorList = new ArrayList<>(Arrays.asList(Color.RED, Color.GREEN, Color.BLUE, Color.ORANGE)) ;
        final Iterator<Color> colors = colorList.iterator();

        for (Player player:game.getWorld().getPlayerList()){

            player.setColor(colors.next());

            int x = player.getCoords().getX()/2;
            int y = player.getCoords().getY()/2;

            g2D.placePlayer(player,new Point2D(x,y));

            g3D.placePlayer(player, new Point2D(COEF,COEF));
            g3D.movePlayer(player,new Point2D(x,y));

        }

        for(Cell[] cells:game.getWorld().getBoard()){
            for (Cell cell:cells){
                if(cell.hasWall()){

                    Point[] boardPos = getRealBoardPos(cell);
                    Point coords = boardPos[0];
                    Direction direction = (Direction) boardPos[1];

                    g2D.setDrawColor(Color.RED);
                    g2D.placeWall(new Point[]{coords, direction});
                    g3D.placeWall(new Point[]{coords, direction});
                }
            }
        }
    }


    //actions lorsque la souris sort de l'ecran
    @FXML
    private void handleCanvasExit(){
        if(!game.getWorld().getBoard()[oldWallPos[2].getY()][oldWallPos[2].getX()].hasWall()) g2D.removeWall(oldWallPos);
    }

    //Action a effectuer quand la souris bouge
    //en gros c les jolis murs rouge et vert
    @FXML
    private void handleHover(MouseEvent event) {
	    Point2D mCoords = getMousePos(event);
        Point coords = getBoardPos(mCoords);

        if(game.getWorld().notInBound(coords) || game.getWorld().getBoard()[coords.getY()][coords.getX()].hasWall()) return;

        assert mCoords != null;
        Point[] wallPos = getWallPositioning(mCoords);

        if (wallPos == null) {
            if(!game.getWorld().getBoard()[oldWallPos[2].getY()][oldWallPos[2].getX()].hasWall()) g2D.removeWall(oldWallPos);
            return;
        }

        Point tempCoords = coords.add(wallPos[1]);
        if(!game.getWorld().notInBound(tempCoords) && game.getWorld().getBoard()[tempCoords.getY()][tempCoords.getX()].hasWall()) return;

        tempCoords = tempCoords.add(wallPos[1]);
        if(!game.getWorld().notInBound(tempCoords) && game.getWorld().getBoard()[tempCoords.getY()][tempCoords.getX()].hasWall()) return;


        if( (coords.getX() ==0 && wallPos[1].equals(new Point(0,1))) || (coords.getY() ==0 && wallPos[1].equals(new Point(1,0)))) return;

        if (oldWallPos[0].equals(wallPos[0]) && oldWallPos[1].equals(wallPos[1])){

            if(Arrays.stream(game.getWorld().getBoard()[coords.getY()][coords.getX()].getCanPlaceWall()).noneMatch(new Direction(wallPos[1])::equals)){
                g2D.setDrawColor(Color.GRAY);
            } else {
                g2D.setDrawColor(Color.rgb(200,50,50,0.2));
            }
            g2D.placeWall(oldWallPos);
        } else {
            if(!game.getWorld().getBoard()[oldWallPos[2].getY()][oldWallPos[2].getX()].hasWall()) g2D.removeWall(oldWallPos);

            oldWallPos = new Point[]{wallPos[0], wallPos[1], coords};
        }
	}

    @FXML
    private void handleClick(MouseEvent event) {
        Point2D mCoords = getMousePos(event);

        Point coords = getBoardPos(mCoords);

        if(!game.getWorld().notInBound(coords) && game.getWorld().getBoard()[coords.getY()][coords.getX()].hasPlayer()){
            //dessine ou enlevle les possibliter de depaclement
            Player player = game.getWorld().getBoard()[coords.getY()][coords.getX()].getPlayer();

            if (!player.equals(game.getCurrentPlayer())) return;

            ArrayList<Point> positionList = player.getPositionList();
            game.getWorld().refreshDirectionsCanMove(player);
            if(onMove != null){
                for (Point point:onMove){
                    g2D.delCanMove(point.divide(2));
                }
                onMove=null;
            } else {

                for (Point point:positionList){
                    g2D.drawCanMove(point.divide(2));
                }
                onMove=positionList;
            }

        } else {
            //si peut bouger bouge sinon efface les mouvement
            if(onMove != null){
                for (Point point:onMove){
                    g2D.delCanMove(point.divide(2));
                }
                if(onMove.contains(coords)){
                    //deplace en 2d et 3d et passe au tour suivant

                    game.nextTurn(new Point[]{coords,null});
                }

                onMove=null;
            }
            //pose un mur si possible
            if (game.getCurrentPlayer().hasWall()) {
                Point coordsBoard = getBoardPos(mCoords);
                assert mCoords != null;
                Point[] wallCoords = getWallPositioning(mCoords);
                if (wallCoords != null){

                    game.nextTurn(new Point[] {coordsBoard,wallCoords[1]});

                }
            }
        }
    }


    public MainController getMainController() {
        return mainController;
    }

    private Point[] getRealBoardPos(Cell cell){
        int x = cell.getWallCell().getWall().getCoords().getX();
        int y = cell.getWallCell().getWall().getCoords().getY();
        Point coords = new Point(x%2==0?x/2:(x+1)/2,y%2==0?y/2:(y+1)/2);
        Direction direction = cell.getWallCell().getWall().getDirection();
        return new Point[]{coords, direction};
    }

    private Point2D getMousePos(MouseEvent event){
        double mouseX = event.getX();
        double mouseY = event.getY();
        try {
            return affine.inverseTransform(mouseX, mouseY);

        } catch (NonInvertibleTransformException e) {
            System.out.println("Could not invert transform");
        }
        return null;
    }

    //retourne la vraie position du mur et sa direction
    public Point[] getWallPositioning(Point2D coords){

        assert coords != null;
        double x =  coords.getX();
        double y =  coords.getY();

        double xF = x-(int) x;
        double yF = y-(int) y;

        int xI = (int)x;
        int yI = (int)y;

        if (xF < GameViewController.LINE_WIDTH/2) {

            return new Point[] {new Point(xI, yI), new Point(0, 1)};
        } else if (xF > 1-GameViewController.LINE_WIDTH/2) {

            return new Point[] {new Point(xI+1, yI), new Point(0, 1)};
        } else if (yF > 1-GameViewController.LINE_WIDTH/2) {

            return new Point[] {new Point(xI, yI+1), new Point(1, 0)};

        } else if (yF < GameViewController.LINE_WIDTH/2) {

            return new Point[] {new Point(xI, yI), new Point(1, 0)};
        }

        return null;
    }

    //retourne la vraie position sur le plateau
    public Point getBoardPos(Point2D coords){
        if (getWallPositioning(coords) != null) {
            Point wallPos = Objects.requireNonNull(getWallPositioning(coords))[0];
            Point wallDir = Objects.requireNonNull(getWallPositioning(coords))[1];
            int delta = 1;
            int gama = 2;
            if (wallDir.equals(new Point(0,1))){
                delta = 2;
                gama = 1;
            }
            int wallPosX = wallPos.getX() == 0 ? 0 : (((wallPos.getX()-1)*2)+gama);
            int wallPosY = wallPos.getY() == 0 ? 0 : (((wallPos.getY()-1)*2)+delta);

            wallPos = new Point(wallPosX,wallPosY);

            return wallPos;
        } else {
            Point mousePosInt = new Point((int) coords.getX(),(int) coords.getY()) ;
            return new Point(mousePosInt.getX()*2,mousePosInt.getY()*2);
        }
    }

    public void updateGui(Action action){
	    if (action.getActionType().equals("MoveAction")){
            MoveAction moveAction= (MoveAction) action;
            Player player = moveAction.getPlayer();

            //getG2D().removePlayer(player,moveAction.getOldPos().divide(2).toPoint2D());
            getG3D().movePlayer(player,moveAction.getNextPosition().divide(2).toPoint2D());
            getG2D().movePlayer(player,moveAction.getOldPos().divide(2).toPoint2D());

        } else if (action.getActionType().equals("WallAction")){
            WallAction wallAction= (WallAction) action;

            getG2D().setDrawColor(Color.RED);
            Point wallPos = wallAction.getWallPos();
            Point wallDir = wallAction.getWallDirection();
            int x = wallPos.getX();
            int y = wallPos.getY();
            Point boardPos = new Point(x%2==0?x/2:(x+1)/2,y%2==0?y/2:(y+1)/2);
            getG2D().placeWall(new Point[] {boardPos,wallDir});
            getG3D().placeWall(new Point[] {boardPos,wallDir});
        }
    }


    public void print(String text){
        Runnable task = () -> {
            if (mainController.getCurrentPage().getLast().equals("GameView")) mainController.getRootLayoutController().setLabel(text);
        };

        Platform.runLater(task);

    }

    public Graphical2DGame getG2D(){
	    return g2D;
    }
    public Graphical3DGame getG3D(){
	    return g3D;
    }
}
