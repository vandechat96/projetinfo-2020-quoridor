package quoridor.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;
import org.json.JSONObject;
import quoridor.model.game.Game;
import quoridor.utils.SaveHandler;

import java.io.File;


public class StartMenuController extends Controller {

	@FXML
	private Button exit;
	@FXML
	private Button newGame;
	@FXML
	private Button options;
	@FXML
	private Button loadGame;


	public StartMenuController(){
	}


    @FXML
    public void handleStartButton(){
	    mainController.showGameSelection();
    }

    @FXML
	private void handleLoad(){
		final FileChooser fileChooser = new FileChooser();
		JSONObject localization = mainController.getLocalization();

		fileChooser.setTitle(localization.getString("Save0"));
		fileChooser.setInitialDirectory(new File(SaveHandler.getSavePath()));
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Quoridor save file", "*.qsf"));
		File file = fileChooser.showOpenDialog(mainController.getPrimaryStage());

		if (file != null && file.exists()){
			Game game = SaveHandler.Load(file);
			if(game != null) mainController.showGameView(game);
		}
	}
	@FXML
	private void handleOptions(){
		mainController.getOptionsMenuController().switchView();
		System.out.println("options");
	}
	@FXML
	private void handleExit(){
		mainController.exitDialog();
	}

	public void setLocalization(){
		JSONObject localization = mainController.getLocalization();
		exit.setText(localization.getString("Exit"));
		newGame.setText(localization.getString("NewGame"));
		options.setText(localization.getString("Options"));
		loadGame.setText(localization.getString("LoadGame"));
	}

}
