package quoridor.controller;

import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class RootLayoutController extends Controller{
	@FXML
	private Button backButton;
	@FXML
	private Label label;

	public Button getBackButton(){
		return backButton;
	}

	public RootLayoutController() {	
	}





	@FXML
	private void onKeyPressed(KeyEvent keyEvent) {
		if (keyEvent.getCode() == KeyCode.ESCAPE) {
			String last= mainController.getCurrentPage().getLast();
			if (last.equals("GameView") || last.equals("GameMenu")){
				GameMenuController gameMenuController = mainController.getGameMenuController();
				if( gameMenuController!= null) gameMenuController.switchView();
			}


		}
	}

	@FXML
	private void handleBack(){
		mainController.back();
	}

	public void setLabel(String coords){
		this.label.setText(coords);
	}

	public void setLocalization(){
		backButton.setText(mainController.getLocalization().getString("Back"));
	}
}
