package quoridor.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import org.json.JSONObject;
import quoridor.utils.SaveHandler;

import java.io.File;
import java.io.IOException;

public class GameMenuController extends Controller{

    @FXML
    private Button resume;
    @FXML
    private Button save;
    @FXML
    private Button options;
    @FXML
    private Button exit;



    @FXML
    private void handleResume(){
        switchView();
    }

    @FXML
    private void handleSave() {
        final FileChooser fileChooser = new FileChooser();
        JSONObject localization = mainController.getLocalization();

        fileChooser.setTitle(localization.getString("Save0"));
        fileChooser.setInitialDirectory(new File(SaveHandler.getSavePath()));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Quoridor save file", "*.qsf"));
        File file = fileChooser.showSaveDialog(mainController.getPrimaryStage());

        if (file != null){
            if (!file.exists()){
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            SaveHandler.Save(mainController.getGame(),file);

        }

        System.out.println("save");
    }

    @FXML
    private void handleOptions(){
        switchView();
        mainController.getOptionsMenuController().switchView();
    }

    @FXML
    private void handleExit(){
        mainController.exitDialog();
    }

    public void switchView(){
        VBox menu = mainController.getGameMenu();
        if (menu == null) return;
        if (menu.isVisible()) {
            menu.toBack();
            menu.setVisible(false);
            menu.setDisable(true);
            mainController.getCurrentPage().removeLast();
        } else {
            menu.toFront();
            menu.setVisible(true);
            menu.setDisable(false);
            mainController.getCurrentPage().add("GameMenu");
        }
    }

    public void setLocalization(){
        JSONObject localization = mainController.getLocalization();
        exit.setText(localization.getString("Exit"));
        save.setText(localization.getString("Save"));
        options.setText(localization.getString("Options"));
        resume.setText(localization.getString("Resume"));
    }
}
