package quoridor.controller;

import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.json.JSONObject;
import quoridor.model.game.Game;

import java.util.ArrayList;


public class GameSelectionController extends Controller {

    @FXML
    VBox vBox;
    @FXML
    Spinner<Integer> taille;
    @FXML
    Spinner<Integer> nbrMurs;

    @Override
    public void setMainController(MainController mainController) {
        super.setMainController(mainController);
        taille.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(9,31,9,2));
        nbrMurs.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0,50,10,1));
    }

    public static class PlayerAttribute {
        public final String name;
        public final String type;
        public PlayerAttribute(String name, String type){
            this.name = name;
            this.type = type;
        }
    }
    @FXML
    private void handleStart(){
        ArrayList<PlayerAttribute> players= new ArrayList<>();

        for(Node object :vBox.getChildren()){
            HBox hObject = (HBox) object;
            ObservableList<Node> childrens = hObject.getChildren();

            CheckBox checkbox = (CheckBox) childrens.get(0);
            if (checkbox.isSelected()){
                TextField textField = (TextField) childrens.get(2);
                String name = textField.getText();
                if(name == null || name.isBlank()) return;
                ChoiceBox<String> choiceBox = (ChoiceBox<String>) childrens.get(3);
                String playerType = choiceBox.getValue();
                if(playerType == null) return;
                JSONObject localization = mainController.getLocalization();

                playerType= playerType.equals(localization.getString("Human"))?"human":playerType.equals(localization.getString("ia0"))?"facile":"dur";

                players.add(new PlayerAttribute(name, playerType));
            }
        }
        if(players.size()==0) return;

        mainController.showGameView(new Game(players,taille.getValue(),nbrMurs.getValue()));
    }

    @Override
    public void setLocalization() {
        JSONObject localization = mainController.getLocalization();
        int i=1;
        for(Node object :vBox.getChildren()){
            HBox hObject = (HBox) object;
            ObservableList<Node> childrens = hObject.getChildren();

            CheckBox checkbox = (CheckBox) childrens.get(0);
            checkbox.setText(localization.getString("Player")+" "+(i++));
            Label label = (Label) childrens.get(1);
            label.setText(localization.getString("Name")+" : ");
            //TextField textField = (TextField) childrens.get(2);
            ChoiceBox<String> choiceBox = (ChoiceBox<String>) childrens.get(3);
            choiceBox.getItems().addAll(localization.getString("Human"),localization.getString("ia0"),localization.getString("ia1"));
        }

    }

}
