package quoridor.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import org.json.JSONObject;

public class OptionsMenuController extends Controller{



    @FXML
    private MenuButton languagesMB;
    @FXML
    private Label languages;
    @FXML
    private Button quit;


    @FXML
    private void handleChangeLanguage(ActionEvent event){
        MenuItem item = (MenuItem) event.getSource();
        mainController.setLocalization(item.getId()+".json");

    }
    @FXML
    private void handleQuit(){
        switchView();
    }

    public void switchView(){
        AnchorPane menu = mainController.getOptionsMenu();
        if (menu == null) return;
        if (menu.isVisible()) {
            menu.toBack();
            menu.setVisible(false);
            menu.setDisable(true);
            mainController.getCurrentPage().removeLast();
        } else {
            menu.toFront();
            menu.setVisible(true);
            menu.setDisable(false);
            mainController.getCurrentPage().add("OptionsMenu");
        }
    }

    public void setLocalization(){
        JSONObject localization = mainController.getLocalization();
        quit.setText(localization.getString("Quit"));
        languages.setText(localization.getString("Languages"));
        languagesMB.setText(localization.getString("Languages"));

    }
}
