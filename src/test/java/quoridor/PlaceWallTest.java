package quoridor;

import org.junit.Test;
import quoridor.model.game.Direction;
import quoridor.model.game.Player;
import quoridor.model.game.Point;
import quoridor.model.game.World;

import static org.junit.Assert.*;


public class PlaceWallTest {
    @Test
    public void basicWallPlacing() {
        World world = new World();
        Player[] players = {};
        world.setPlayerList(players);
        world.refreshCanPlaceWall();

        //normal wall vertical
        world.playerPlaceWall(new Point(7, 6), new Direction("B"));
        world.refreshCanPlaceWall();
        assertTrue(world.getBoard()[6][7].hasWall());

        world.playerPlaceWall(new Point(5, 6), new Direction("H"));
        world.refreshCanPlaceWall();
        assertTrue(world.getBoard()[6][5].hasWall());

        //normal wall horizontal
        world.playerPlaceWall(new Point(8, 9), new Direction("D"));
        world.refreshCanPlaceWall();
        assertTrue(world.getBoard()[9][8].hasWall());

        world.playerPlaceWall(new Point(6, 9), new Direction("G"));
        world.refreshCanPlaceWall();
        assertTrue(world.getBoard()[9][6].hasWall());

        //cross wall vertical
        world.playerPlaceWall(new Point(6, 7), new Direction("D"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[7][6].hasWall());

        world.playerPlaceWall(new Point(8, 7), new Direction("G"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[7][8].hasWall());

        //cross wall horizontal
        world.playerPlaceWall(new Point(9, 8), new Direction("B"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[8][9].hasWall());

        world.playerPlaceWall(new Point(9, 10), new Direction("H"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[10][9].hasWall());

        //on case wall vertical
        world.playerPlaceWall(new Point(2, 2), new Direction("B"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[2][2].hasWall());

        world.playerPlaceWall(new Point(2, 2), new Direction("H"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[2][2].hasWall());

        //on case wall horizontal
        world.playerPlaceWall(new Point(2, 2), new Direction("D"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[2][2].hasWall());

        world.playerPlaceWall(new Point(2, 2), new Direction("G"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[2][2].hasWall());

        //middle wall vertical
        world.playerPlaceWall(new Point(1, 1), new Direction("B"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[1][1].hasWall());

        world.playerPlaceWall(new Point(1, 1), new Direction("H"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[1][1].hasWall());

        //middle wall horizontal
        world.playerPlaceWall(new Point(1, 1), new Direction("D"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[1][1].hasWall());

        world.playerPlaceWall(new Point(1, 1), new Direction("G"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[1][1].hasWall());

        //outside wall vertical
        world.playerPlaceWall(new Point(1, 0), new Direction("H"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[0][1].hasWall());

        world.playerPlaceWall(new Point(1, 16), new Direction("B"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[16][1].hasWall());

        //outside wall horizontal
        world.playerPlaceWall(new Point(0, 1), new Direction("G"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[1][0].hasWall());

        world.playerPlaceWall(new Point(16, 1), new Direction("D"));
        world.refreshCanPlaceWall();
        assertFalse(world.getBoard()[1][16].hasWall());
    }

    @Test
    public void notBlockingPlayerWallPlacing() {
        World world = new World();

        Point[] p1WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p1WinPos[x] = new Point(x * 2, 16);
        }

        Player p1 = new Player("1", new Point(8, 0), p1WinPos);

        Point[] p2WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p2WinPos[x] = new Point(x * 2,0);
        }

        Player p2 = new Player("2", new Point(8, 16), p2WinPos);

        Player[] players = {p1, p2};
        world.setPlayerList(players);

        world.spawnPlayers();
        world.refreshCanPlaceWall();

        world.placeWall(new Point(9, 0), new Direction("B"));
        world.placeWall(new Point(9, 4), new Direction("B"));
        world.placeWall(new Point(9, 8), new Direction("B"));
        world.placeWall(new Point(9, 12), new Direction("B"));
        world.placeWall(new Point(10, 15), new Direction("D"));
        world.placeWall(new Point(13, 16), new Direction("H"));

        assertTrue(world.getBoard()[16][13].hasWall());

        world.placeWall(new Point(7, 0), new Direction("B"));
        world.placeWall(new Point(7, 4), new Direction("B"));
        world.placeWall(new Point(7, 8), new Direction("B"));
        world.placeWall(new Point(7, 12), new Direction("B"));
        world.placeWall(new Point(6, 15), new Direction("G"));
        world.placeWall(new Point(3, 16), new Direction("H"));

        assertTrue(world.getBoard()[16][3].hasWall());
    }

    @Test
    public void blockingPlayerWallPlacing(){
        World world = new World();

        Point[] p1WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p1WinPos[x] = new Point(x * 2, 16);
        }

        Player p1 = new Player("1", new Point(8, 0), p1WinPos);

        Point[] p2WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p2WinPos[x] = new Point(x * 2, 0);
        }

        Player p2 = new Player("2", new Point(8, 16), p2WinPos);

        Player[] players = {p1, p2};
        world.setPlayerList(players);

        world.spawnPlayers();
        world.refreshCanPlaceWall();

        world.placeWall(new Point(0,9), new Direction("D"));
        world.refreshCanPlaceWall();
        world.placeWall(new Point(4,9), new Direction("D"));
        world.refreshCanPlaceWall();
        world.placeWall(new Point(8,9), new Direction("D"));
        world.refreshCanPlaceWall();
        world.placeWall(new Point(12,9), new Direction("D"));
        world.refreshCanPlaceWall();
        world.placeWall(new Point(15,8), new Direction("B"));
        world.refreshCanPlaceWall();
        assertFalse(world.checkCanPlaceWallPlayer(new Point(14,7), new Direction("D")));
    }

    @Test
    public void surroundPlayerWallPlacing(){
        World world = new World();

        Point[] p1WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p1WinPos[x] = new Point(x * 2, 16);
        }

        Player p1 = new Player("1", new Point(8, 8), p1WinPos);

        Point[] p2WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p2WinPos[x] = new Point(x * 2, 0);
        }

        Player p2 = new Player("2", new Point(8, 16), p2WinPos);

        Player[] players = {p1, p2};
        world.setPlayerList(players);

        world.spawnPlayers();
        world.refreshCanPlaceWall();

        world.placeWall(new Point(8,7), new Direction("D"));
        world.refreshCanPlaceWall();
        world.placeWall(new Point(7,8), new Direction("B"));
        world.refreshCanPlaceWall();
        world.placeWall(new Point(9,8), new Direction("B"));
        world.refreshCanPlaceWall();
        assertFalse(world.checkCanPlaceWallPlayer(new Point(8,11), new Direction("D")));
    }

    @Test
    public void surroundEdgePlayerWallPlacing(){
        World world = new World();

        Point[] p1WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p1WinPos[x] = new Point(x * 2, 16);
        }

        Player p1 = new Player("1", new Point(8, 0), p1WinPos);

        Point[] p2WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p2WinPos[x] = new Point(x * 2, 0);
        }

        Player p2 = new Player("2", new Point(0, 8), p2WinPos);

        Player[] players = {p1, p2};
        world.setPlayerList(players);

        world.spawnPlayers();
        world.refreshCanPlaceWall();

        world.placeWall(new Point(7,0), new Direction("B"));
        world.refreshCanPlaceWall();
        world.placeWall(new Point(9,0), new Direction("B"));
        world.refreshCanPlaceWall();
        assertFalse(world.checkCanPlaceWallPlayer(new Point(6,3), new Direction("D")));

        world.placeWall(new Point(0,7), new Direction("D"));
        world.refreshCanPlaceWall();
        world.placeWall(new Point(0,9), new Direction("D"));
        world.refreshCanPlaceWall();
        assertFalse(world.checkCanPlaceWallPlayer(new Point(3,6), new Direction("B")));

    }
}
