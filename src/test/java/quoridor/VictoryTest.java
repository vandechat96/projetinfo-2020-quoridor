package quoridor;

import org.junit.Test;
import quoridor.model.game.Player;
import quoridor.model.game.Point;
import quoridor.model.game.World;

import static org.junit.Assert.*;

public class VictoryTest {

    @Test
    public void notPlayerVictoryTest () {
        World world = new World();

        Point[] p1WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p1WinPos[x] = new Point(x * 2, 16);
        }

        Player p1 = new Player("1", new Point(8, 0), p1WinPos);

        Player[] players = {p1};
        world.setPlayerList(players);
        world.spawnPlayers();

        assertFalse(world.checkWin(p1));
    }

    @Test
    public void playerVictoryTest () {
        World world = new World();

        Point[] p1WinPos = new Point[9];
        for (int x = 0; x < 9; x++) {
            p1WinPos[x] = new Point(x * 2, 16);
        }

        Player p1 = new Player("1", new Point(8, 16), p1WinPos);

        Player[] players = {p1};
        world.setPlayerList(players);
        world.spawnPlayers();

        assertTrue(world.checkWin(p1));
    }
}
