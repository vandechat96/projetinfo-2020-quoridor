package quoridor;

import org.junit.Test;
import quoridor.model.game.Direction;
import quoridor.model.game.Player;
import quoridor.model.game.Point;
import quoridor.model.game.World;

import static org.junit.Assert.*;

public class BasicMovementTest {

    @Test
    public void basicFreeMovementTest() {
        World world = new World();
        Point[] p1WinPos = new Point[9];
        p1WinPos[0] = new Point(-1, -1);
        Player p1 = new Player("1", new Point(8, 8), p1WinPos);

        Player[] players = {p1};
        world.setPlayerList(players);
        world.spawnPlayers();
        world.refreshCanPlaceWall();

        Direction playerDirection;
        Point playerMove;

        //Test direction Haut
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("H");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        assertEquals(4,p1.getPositionList().size());
        assertEquals(8, p1.getCoords().getX());
        assertEquals(6, p1.getCoords().getY());
        assertNull(world.getBoard()[8][8].getPlayer());
        assertEquals(p1, world.getBoard()[6][8].getPlayer());


        //Test direction Droite
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("D");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        assertEquals(4,p1.getPositionList().size());
        assertEquals(10, p1.getCoords().getX());
        assertEquals(6, p1.getCoords().getY());
        assertNull(world.getBoard()[6][8].getPlayer());
        assertEquals(p1, world.getBoard()[6][10].getPlayer());

        world.refreshDirectionsCanMove(p1);

        //Test direction Bas
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("B");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        assertEquals(4,p1.getPositionList().size());
        assertEquals(10, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertNull(world.getBoard()[6][10].getPlayer());
        assertEquals(p1, world.getBoard()[8][10].getPlayer());

        world.refreshDirectionsCanMove(p1);

        //Test direction Gauche
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("G");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        assertEquals(4,p1.getPositionList().size());
        assertEquals(8, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertNull(world.getBoard()[8][10].getPlayer());
        assertEquals(p1, world.getBoard()[8][8].getPlayer());
    }

    @Test
    public void basicMovementEdgeTest(){
        World world = new World();
        Point[] p1WinPos = new Point[9];
        p1WinPos[0] = new Point(-1, -1);
        Player p1 = new Player("1", new Point(8, 0), p1WinPos);

        Player[] players = {p1};
        world.setPlayerList(players);
        world.spawnPlayers();
        world.refreshCanPlaceWall();

        Direction playerDirection;
        Point playerMove;

        //Test direction Haut
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("H");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        assertEquals(3,p1.getPositionList().size());
        assertEquals(8, p1.getCoords().getX());
        assertEquals(0, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[0][8].getPlayer());

        world.movePlayer(p1, new Point(16,8));

        //Test direction Droite
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("D");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        assertEquals(3,p1.getPositionList().size());
        assertEquals(16, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[8][16].getPlayer());

        world.movePlayer(p1, new Point(8,16));

        //Test direction Bas
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("B");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        assertEquals(3,p1.getPositionList().size());
        assertEquals(8, p1.getCoords().getX());
        assertEquals(16, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[16][8].getPlayer());

        world.movePlayer(p1, new Point(0,8));

        //Test direction Gauche
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("G");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        assertEquals(3,p1.getPositionList().size());
        assertEquals(0, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[8][0].getPlayer());
    }

    @Test
    public void basicMoveToWall(){
        World world = new World();
        Point[] p1WinPos = new Point[9];
        p1WinPos[0] = new Point(-1, -1);
        Player p1 = new Player("1", new Point(8, 8), p1WinPos);

        Player[] players = {p1};
        world.setPlayerList(players);
        world.spawnPlayers();
        world.refreshCanPlaceWall();

        Direction playerDirection;
        Point playerMove;

        //Test direction Haut
        world.placeWall(new Point(6,7),new Direction("D"));

        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("H");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        world.removeWall(world.getWallAtPosition(new Point(6,7)));

        assertEquals(3,p1.getPositionList().size());
        assertEquals(8, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[8][8].getPlayer());

        //Test direction Droite
        world.placeWall(new Point(9,6),new Direction("B"));

        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("D");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        world.removeWall(world.getWallAtPosition(new Point(9,6)));

        assertEquals(3,p1.getPositionList().size());
        assertEquals(8, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[8][8].getPlayer());

        //Test direction Bas
        world.placeWall(new Point(10,9),new Direction("G"));

        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("B");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }
        world.removeWall(world.getWallAtPosition(new Point(10,9)));

        assertEquals(3,p1.getPositionList().size());
        assertEquals(8, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[8][8].getPlayer());

        //Test direction Gauche
        world.placeWall(new Point(7,10),new Direction("H"));

        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("G");
        playerMove = p1.getCoords().add(playerDirection.multiply(2));
        if (p1.getPositionList().contains(playerMove)) {
            world.movePlayer(p1, playerMove);
        }

        world.removeWall(world.getWallAtPosition(new Point(7,10)));

        assertEquals(3,p1.getPositionList().size());
        assertEquals(8, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[8][8].getPlayer());
    }
}
