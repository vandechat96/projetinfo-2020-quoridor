package quoridor;

import org.junit.Test;
import quoridor.model.game.Direction;
import quoridor.model.game.Player;
import quoridor.model.game.Point;
import quoridor.model.game.World;

import static org.junit.Assert.*;


public class SpecialMovementTest {

    @Test
    public void jumpPlayerMovementTest(){
        World world = new World();
        Point[] p1WinPos = new Point[9];
        Point[] p2WinPos = new Point[9];

        p1WinPos[0] = new Point(-1, -1);
        Player p1 = new Player("1", new Point(8, 10), p1WinPos);
        p2WinPos[0] = new Point(-1, -1);
        Player p2 = new Player("2", new Point(8, 8), p2WinPos);

        Player[] players = {p1,p2};
        world.setPlayerList(players);
        world.spawnPlayers();

        Direction playerDirection;
        Point playerMoveWithPlayer;

        //Haut
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("H");
        playerMoveWithPlayer = p1.getCoords().add(playerDirection.multiply(4));

        assertEquals(4, p1.getPositionList().size());

        if (p1.getPositionList().contains(playerMoveWithPlayer)) {
            world.movePlayer(p1, playerMoveWithPlayer);
        }

        assertEquals(8, p1.getCoords().getX());
        assertEquals(6, p1.getCoords().getY());
        assertNull(world.getBoard()[10][8].getPlayer());
        assertEquals(p1, world.getBoard()[6][8].getPlayer());


        //Droite
        world.movePlayer(p1, new Point(6,8));

        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("D");
        playerMoveWithPlayer = p1.getCoords().add(playerDirection.multiply(4));

        assertEquals(4, p1.getPositionList().size());

        if (p1.getPositionList().contains(playerMoveWithPlayer)) {
            world.movePlayer(p1, playerMoveWithPlayer);
        }

        assertEquals(10, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertNull(world.getBoard()[8][6].getPlayer());
        assertEquals(p1, world.getBoard()[8][10].getPlayer());
    }

    @Test
    public void notJumpPlayerMovementTest(){
        World world = new World();
        Point[] p1WinPos = new Point[9];
        Point[] p2WinPos = new Point[9];
        Point[] p3WinPos = new Point[9];

        p1WinPos[0] = new Point(-1, -1);
        Player p1 = new Player("1", new Point(8, 10), p1WinPos);
        p2WinPos[0] = new Point(-1, -1);
        Player p2 = new Player("2", new Point(8, 8), p2WinPos);
        p3WinPos[0] = new Point(-1, -1);
        Player p3 = new Player("2", new Point(8, 6), p3WinPos);

        Player[] players = {p1,p2,p3};
        world.setPlayerList(players);
        world.spawnPlayers();

        Direction playerDirection;
        Point playerMoveWithPlayer;

        //Haut player
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("H");
        playerMoveWithPlayer = p1.getCoords().add(playerDirection.multiply(4));

        assertEquals(5, p1.getPositionList().size());

        if (p1.getPositionList().contains(playerMoveWithPlayer)) {
            world.movePlayer(p1, playerMoveWithPlayer);
        }

        assertEquals(8, p1.getCoords().getX());
        assertEquals(10, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[10][8].getPlayer());

        //Haut wall
        world.movePlayer(p3, new Point(0,0));
        world.placeWall(new Point(8,7),new Direction("G"));

        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("H");
        playerMoveWithPlayer = p1.getCoords().add(playerDirection.multiply(4));

        assertEquals(5, p1.getPositionList().size());

        if (p1.getPositionList().contains(playerMoveWithPlayer)) {
            world.movePlayer(p1, playerMoveWithPlayer);
        }

        assertEquals(8, p1.getCoords().getX());
        assertEquals(10, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[10][8].getPlayer());
    }

    @Test
    public void diagonalPlayerMovementTest(){
        World world = new World();
        Point[] p1WinPos = new Point[9];
        Point[] p2WinPos = new Point[9];
        Point[] p3WinPos = new Point[9];

        p1WinPos[0] = new Point(-1, -1);
        Player p1 = new Player("1", new Point(8, 10), p1WinPos);
        p2WinPos[0] = new Point(-1, -1);
        Player p2 = new Player("2", new Point(8, 8), p2WinPos);
        p3WinPos[0] = new Point(-1, -1);
        Player p3 = new Player("2", new Point(8, 6), p3WinPos);

        Player[] players = {p1,p2,p3};
        world.setPlayerList(players);
        world.spawnPlayers();

        Direction playerDirection;
        Point playerMoveWithPlayer;

        //Haut Droite player
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("HD");
        playerMoveWithPlayer = p1.getCoords().add(playerDirection.multiply(2));

        assertEquals(5, p1.getPositionList().size());

        if (p1.getPositionList().contains(playerMoveWithPlayer)) {
            world.movePlayer(p1, playerMoveWithPlayer);
        }

        assertEquals(10, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertNull(world.getBoard()[10][8].getPlayer());
        assertEquals(p1, world.getBoard()[8][10].getPlayer());

        //Haut Droite wall
        world.movePlayer(p3, new Point(0,0));
        world.movePlayer(p1, new Point(8,10));
        world.placeWall(new Point(8,7),new Direction("G"));

        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("HD");
        playerMoveWithPlayer = p1.getCoords().add(playerDirection.multiply(2));

        assertEquals(5, p1.getPositionList().size());

        if (p1.getPositionList().contains(playerMoveWithPlayer)) {
            world.movePlayer(p1, playerMoveWithPlayer);
        }

        assertEquals(10, p1.getCoords().getX());
        assertEquals(8, p1.getCoords().getY());
        assertNull(world.getBoard()[10][8].getPlayer());
        assertEquals(p1, world.getBoard()[8][10].getPlayer());
    }

    @Test
    public void notDiagonalPlayerMovementTest(){
        World world = new World();
        Point[] p1WinPos = new Point[9];
        Point[] p2WinPos = new Point[9];
        Point[] p3WinPos = new Point[9];

        p1WinPos[0] = new Point(-1, -1);
        Player p1 = new Player("1", new Point(8, 10), p1WinPos);
        p2WinPos[0] = new Point(-1, -1);
        Player p2 = new Player("2", new Point(8, 8), p2WinPos);
        p3WinPos[0] = new Point(-1, -1);
        Player p3 = new Player("2", new Point(8, 6), p3WinPos);

        Player[] players = {p1,p2,p3};
        world.setPlayerList(players);
        world.spawnPlayers();

        Direction playerDirection;
        Point playerMoveWithPlayer;

        world.placeWall(new Point(7,8),new Direction("B"));
        world.placeWall(new Point(9,8),new Direction("B"));

        //Haut Droite player
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("HD");
        playerMoveWithPlayer = p1.getCoords().add(playerDirection.multiply(2));

        assertEquals(1, p1.getPositionList().size());

        if (p1.getPositionList().contains(playerMoveWithPlayer)) {
            world.movePlayer(p1, playerMoveWithPlayer);
        }

        assertEquals(8, p1.getCoords().getX());
        assertEquals(10, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[10][8].getPlayer());

        //Haut Droite wall
        world.movePlayer(p3, new Point(0,0));
        world.movePlayer(p1, new Point(8,10));
        world.placeWall(new Point(8,7),new Direction("G"));

        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("HD");
        playerMoveWithPlayer = p1.getCoords().add(playerDirection.multiply(2));

        assertEquals(1, p1.getPositionList().size());

        if (p1.getPositionList().contains(playerMoveWithPlayer)) {
            world.movePlayer(p1, playerMoveWithPlayer);
        }

        assertEquals(8, p1.getCoords().getX());
        assertEquals(10, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[10][8].getPlayer());
    }


    @Test
    public void notJumpPlayerBoarderMovementTest() {
        World world = new World();
        Point[] p1WinPos = new Point[9];
        Point[] p2WinPos = new Point[9];

        p1WinPos[0] = new Point(-1, -1);
        Player p1 = new Player("1", new Point(8, 2), p1WinPos);
        p2WinPos[0] = new Point(-1, -1);
        Player p2 = new Player("2", new Point(8, 0), p2WinPos);

        Player[] players = {p1, p2};
        world.setPlayerList(players);
        world.spawnPlayers();

        Direction playerDirection;
        Point playerMoveWithPlayer;

        //Haut player
        world.refreshDirectionsCanMove(p1);
        playerDirection = new Direction("H");
        playerMoveWithPlayer = p1.getCoords().add(playerDirection.multiply(4));

        assertEquals(5, p1.getPositionList().size());

        if (p1.getPositionList().contains(playerMoveWithPlayer)) {
            world.movePlayer(p1, playerMoveWithPlayer);
        }

        assertEquals(8, p1.getCoords().getX());
        assertEquals(2, p1.getCoords().getY());
        assertEquals(p1, world.getBoard()[2][8].getPlayer());
    }
}
